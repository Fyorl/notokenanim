# NoTokenAnim
A [Foundry VTT](http://foundryvtt.com/) module that provides an option to remove token movement animations.

Requires Foundry VTT version 0.4.4 or greater.
