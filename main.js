'use strict';

Hooks.on('ready', () => {
	game.settings.register('notokenanim', 'enabled', {
		name: 'Disable Token Movement Animations',
		default: false,
		type: Boolean,
		scope: 'client',
		config: true,
		hint: 'Dragging and dropping tokens around the map will resolve instantly instead of sliding.'
	});
});

Hooks.on('preUpdateToken', (doc, change, options) => {
	if (game.settings.get('notokenanim', 'enabled')) {
		options.animate = false;
	}
});
